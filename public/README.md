# Software Studio 2018 Spring Assignment 02 小朋友下樓梯

## 小朋友下樓梯
<img src="example01.png" width="700px" height="500px"></img>

## Goal
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Complete a game "小朋友下樓梯" by Phaser. (JavaScript or TypeScript)
3. Your game should reach the basic requirements.
4. You can download needed materials from some open source webpage to beautify the appearance.
5. Commit to "your" project repository and deploy to Gitlab page.
6. **Report which items you have done and describing other functions or feature in REABME.md.**

## Scoring 
|                                              Item                                              | Score |
|:----------------------------------------------------------------------------------------------:|:-----:|
| A complete game process: start menu => game view => game over => quit or play again            |  20%  |
| Your game should follow the basic rules of  "小朋友下樓梯".                                    |  15%  |
|         All things in your game should have correct physical properties and behaviors.         |  15%  |
| Set up some interesting traps or special mechanisms. .(at least 2 different kinds of platform) |  10%  |
| Add some additional sound effects and UI to enrich your game.                                  |  10%  |
| Store player's name and score in firebase real-time database, and add a leaderboard to your game.        |  10%  |
| Appearance (subjective)                                                                        |  10%  |
| Other creative features in your game (describe on README.md)                                   |  10%  |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/Assignment_02**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, .ts, etc.
    * source files
* **Deadline: 2018/05/24 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

## Report
* Login Page
    * 與上次midterm project的方式一致製作這次的登入頁面。
    <img src="login.png">
* Menu State
    * 用Phaser的State實作這次的menu。
    <img src="menu.png">
* Game State
    * 遊戲介面。裡面包含寫入Firebase資料庫的Function。
    <img src="game.png">
* Two Player State
    <img src="game2.png">
* Leaderboard State
    * 從Firebase拿取資料，再利用phaser寫入遊戲畫面。
    <img src="leaderboard.png">
* Pause During The Game
    <img src="gamepause.png">
* Features Description
    * Two Players
        * 另外實作一個player2去實現2 players的功能。其方法與player1一致，只是記得要增加player1與player2之間的碰撞。
    * Pause During The Game
        * 增加一個boolean var 讓其進入update function之後可以判斷是否為暫停狀態，若為已經按下暫停的狀態的話，update將不再update其他資訊並且將玩家速度設置為0，達到pause的效果。
