var game = new Phaser.Game(600, 700, Phaser.AUTO, 'canvas');


var firstplace_name = "null";
var secondplace_name = "null";
var thirdplace_name = "null";
var firstplace_record = 0;
var secondplace_record = 0;
var thirdplace_record = 0;
var firstplace_time = "null";
var secondplace_time = "null";
var thirdplace_time = "null";
var number_of_users = 0;

var player;
var player2;
var keyboard;

var platforms = [];

var leftWall;
var rightWall;
var ceiling;
var leftWall2;
var rightWall2;
var ceiling2;
var cover;
var Coverphoto;

var text1;
var text2;
var text3;
var winner_text;
var resume_text;
var menu_StartGame;
var menu_2Players;
var menu_leaderboard;
// var note_player1;
// var note_player2;
var leaderboard_firstplace;
var leaderboard_secondplace;
var leaderboard_thirdplace;
var leaderboard_text;
var leaderboard_firstplace_time;
var leaderboard_secondplace_time;
var leaderboard_thirdplace_time;

var select_one_in_menu = 0;

var distance = 0;
var distance2 = 0;
var status = 'running';
var lastTime = 0;
var tmpTime = 0;
var PressPause = false;
var menumusic;
var gameovermusic;
var jumpmusic;
var runningmusic;
var fakemusic;
var springmusic;
var leaderboardmusic;
var selectmusic;
var screammusic;

var menu_style = { fill: '#0000ff', fontSize: '30px' };
var unselect_style = { fill: '#ffffff', fontSize: '30px' };

var Menu_State = {
    preload: function () {
        game.load.crossOrigin = 'anonymous';
        game.load.spritesheet('player', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/player.png', 32, 32);
        game.load.spritesheet('player2', 'assets/player.png', 32, 32);
        game.load.image('wall', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/wall.png');
        game.load.image('ceiling', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/ceiling.png');
        game.load.image('normal', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/normal.png');
        game.load.image('nails', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/nails.png');
        game.load.image('powerstation', 'assets/powerstation.jpg');
        game.load.image('coverphoto', 'assets/spiral_stairs_vatican_museums-wallpaper-2560x1440.jpg')
        game.load.spritesheet('conveyorRight', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/conveyor_right.png', 96, 16);
        game.load.spritesheet('conveyorLeft', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/conveyor_left.png', 96, 16);
        game.load.spritesheet('trampoline', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/trampoline.png', 96, 22);
        game.load.spritesheet('fake', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/fake.png', 96, 36);
        game.load.audio('menu_music', 'assets/y2mate.com - motivational_background_music_J0BPoofmPkw.mp3');
        game.load.audio('gameover_music', 'assets/falling2a.mp3');
        game.load.audio('spring_music', 'assets/question.mp3');
        game.load.audio('leaderboard_music', 'assets/powerstation_main.mp3');
        game.load.audio('select_music', 'assets/button04b.mp3');
        game.load.audio('scream_music','assets/scream.mp3');
    },

    create: function () {
        keyboard = game.input.keyboard.addKeys({
            'enter': Phaser.Keyboard.ENTER,
            'up': Phaser.Keyboard.UP,
            'down': Phaser.Keyboard.DOWN,
            'left': Phaser.Keyboard.LEFT,
            'right': Phaser.Keyboard.RIGHT,
            'w': Phaser.Keyboard.W,
            'a': Phaser.Keyboard.A,
            's': Phaser.Keyboard.S,
            'd': Phaser.Keyboard.D,
            'P': Phaser.Keyboard.P,
            'R': Phaser.Keyboard.R,
            'Space': Phaser.Keyboard.SPACEBAR,
            'Esc': Phaser.Keyboard.ESC
        });
        menumusic = game.add.audio('menu_music');
        leaderboardmusic = game.add.audio('leaderboard_music', 1, true);
        selectmusic = game.add.audio('select_music');
        screammusic = game.add.audio('scream_music');
        menumusic.play();
        Coverphoto = game.add.image(0, 0, 'coverphoto');
        Coverphoto.scale.setTo(0.5, 0.5);
        menu_StartGame = game.add.text(game.width / 2 - 100, game.height / 2, 'Start', menu_style);
        menu_2Players = game.add.text(game.width / 2 - 100, game.height / 2 + 40, '2 Players', unselect_style);
        menu_leaderboard = game.add.text(game.width / 2 - 100, game.height / 2 + 80, 'Leaderboard', unselect_style);
        this.createBounders();
        getranking();
        if (select_one_in_menu == 0) {
            menu_StartGame = game.add.text(game.width / 2 - 100, game.height / 2, 'Start', menu_style);
            menu_2Players = game.add.text(game.width / 2 - 100, game.height / 2 + 40, '2 Players', unselect_style);
            menu_leaderboard = game.add.text(game.width / 2 - 100, game.height / 2 + 80, 'Leaderboard', unselect_style);
        } else if (select_one_in_menu == 1) {
            menu_StartGame = game.add.text(game.width / 2 - 100, game.height / 2, 'Start', unselect_style);
            menu_2Players = game.add.text(game.width / 2 - 100, game.height / 2 + 40, '2 Players', menu_style);
            menu_leaderboard = game.add.text(game.width / 2 - 100, game.height / 2 + 80, 'Leaderboard', unselect_style);
        } else if (select_one_in_menu == 2) {
            menu_StartGame = game.add.text(game.width / 2 - 100, game.height / 2, 'Start', unselect_style);
            menu_2Players = game.add.text(game.width / 2 - 100, game.height / 2 + 40, '2 Players', unselect_style);
            menu_leaderboard = game.add.text(game.width / 2 - 100, game.height / 2 + 80, 'Leaderboard', menu_style);
        }
        game.input.keyboard.addKey(Phaser.Keyboard.UP).onDown.add(this.previosoption, this);
        game.input.keyboard.addKey(Phaser.Keyboard.DOWN).onDown.add(this.nextoption, this);
        game.input.keyboard.addKey(Phaser.Keyboard.W).onDown.add(this.previosoption, this);
        game.input.keyboard.addKey(Phaser.Keyboard.S).onDown.add(this.nextoption, this);
        var EnterKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        EnterKey.onDown.add(this.startgame, this);
    },
    createBounders: function () {
        leftWall = game.add.sprite(0, 0, 'wall');
        game.physics.arcade.enable(leftWall);
        leftWall.body.immovable = true;

        rightWall = game.add.sprite(583, 0, 'wall');
        game.physics.arcade.enable(rightWall);
        rightWall.body.immovable = true;

        leftWall2 = game.add.sprite(0, 400, 'wall');
        game.physics.arcade.enable(leftWall2);
        leftWall2.body.immovable = true;

        rightWall2 = game.add.sprite(583, 400, 'wall');
        game.physics.arcade.enable(rightWall2);
        rightWall2.body.immovable = true;
    },
    nextoption: function () {
        selectmusic.play();
        if (select_one_in_menu == 0) {
            select_one_in_menu = 1;
            menu_StartGame = game.add.text(game.width / 2 - 100, game.height / 2, 'Start', unselect_style);
            menu_2Players = game.add.text(game.width / 2 - 100, game.height / 2 + 40, '2 Players', menu_style);
            menu_leaderboard = game.add.text(game.width / 2 - 100, game.height / 2 + 80, 'Leaderboard', unselect_style);
        } else if (select_one_in_menu == 1) {
            select_one_in_menu = 2;
            menu_StartGame = game.add.text(game.width / 2 - 100, game.height / 2, 'Start', unselect_style);
            menu_2Players = game.add.text(game.width / 2 - 100, game.height / 2 + 40, '2 Players', unselect_style);
            menu_leaderboard = game.add.text(game.width / 2 - 100, game.height / 2 + 80, 'Leaderboard', menu_style);
        } else if (select_one_in_menu == 2) {
            select_one_in_menu = 0;
            menu_StartGame = game.add.text(game.width / 2 - 100, game.height / 2, 'Start', menu_style);
            menu_2Players = game.add.text(game.width / 2 - 100, game.height / 2 + 40, '2 Players', unselect_style);
            menu_leaderboard = game.add.text(game.width / 2 - 100, game.height / 2 + 80, 'Leaderboard', unselect_style);
        }
    },
    previosoption: function () {
        selectmusic.play();
        if (select_one_in_menu == 0) {
            select_one_in_menu = 2;
            menu_StartGame = game.add.text(game.width / 2 - 100, game.height / 2, 'Start', unselect_style);
            menu_2Players = game.add.text(game.width / 2 - 100, game.height / 2 + 40, '2 Players', unselect_style);
            menu_leaderboard = game.add.text(game.width / 2 - 100, game.height / 2 + 80, 'Leaderboard', menu_style);
        } else if (select_one_in_menu == 1) {
            select_one_in_menu = 0;
            menu_StartGame = game.add.text(game.width / 2 - 100, game.height / 2, 'Start', menu_style);
            menu_2Players = game.add.text(game.width / 2 - 100, game.height / 2 + 40, '2 Players', unselect_style);
            menu_leaderboard = game.add.text(game.width / 2 - 100, game.height / 2 + 80, 'Leaderboard', unselect_style);
        } else if (select_one_in_menu == 2) {
            select_one_in_menu = 1;
            menu_StartGame = game.add.text(game.width / 2 - 100, game.height / 2, 'Start', unselect_style);
            menu_2Players = game.add.text(game.width / 2 - 100, game.height / 2 + 40, '2 Players', menu_style);
            menu_leaderboard = game.add.text(game.width / 2 - 100, game.height / 2 + 80, 'Leaderboard', unselect_style);
        }
    },
    startgame: function () {
        menumusic.stop();
        if (select_one_in_menu == 0) {
            game.state.start('play');
        } else if (select_one_in_menu == 1) {
            game.state.start('play2');
        } else if (select_one_in_menu == 2) {
            game.state.start('leaderboard');
        }

    }
}
var Play_State = {
    preload: function () {
        game.load.crossOrigin = 'anonymous';
        game.load.spritesheet('player', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/player.png', 32, 32);
        game.load.image('wall', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/wall.png');
        game.load.image('ceiling', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/ceiling.png');
        game.load.image('normal', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/normal.png');
        game.load.image('nails', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/nails.png');
        game.load.spritesheet('conveyorRight', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/conveyor_right.png', 96, 16);
        game.load.spritesheet('conveyorLeft', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/conveyor_left.png', 96, 16);
        game.load.spritesheet('trampoline', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/trampoline.png', 96, 22);
        game.load.spritesheet('fake', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/fake.png', 96, 36);
        game.load.audio('menu_music', 'assets/y2mate.com - motivational_background_music_J0BPoofmPkw.mp3');
        game.load.audio('gameover_music', 'assets/falling2a.mp3');
        game.load.audio('jump_music', 'assets/jump.mp3');
        game.load.audio('running_music', 'assets/running_in_a_hall.mp3');
        game.load.audio('fake_music', 'assets/putting_a_large_bag2.mp3');
        game.load.audio('spring_music', 'assets/question.mp3');
        game.load.audio('scream_music','assets/scream.mp3');
    },

    create: function () {
        gameovermusic = game.add.audio('gameover_music');
        jumpmusic = game.add.audio('jump_music');
        runningmusic = game.add.audio('running_music');
        fakemusic = game.add.audio('fake_music');
        springmusic = game.add.audio('spring_music');
        screammusic = game.add.audio('scream_music');
        keyboard = game.input.keyboard.addKeys({
            'enter': Phaser.Keyboard.ENTER,
            'up': Phaser.Keyboard.UP,
            'down': Phaser.Keyboard.DOWN,
            'left': Phaser.Keyboard.LEFT,
            'right': Phaser.Keyboard.RIGHT,
            'w': Phaser.Keyboard.W,
            'a': Phaser.Keyboard.A,
            's': Phaser.Keyboard.S,
            'd': Phaser.Keyboard.D,
            'P': Phaser.Keyboard.P,
            'R': Phaser.Keyboard.R,
            'Space': Phaser.Keyboard.SPACEBAR,
            'Esc': Phaser.Keyboard.ESC
        });
        game.input.keyboard.addKey(Phaser.Keyboard.ESC).onDown.add(this.back_to_menu, this);
        game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR).onDown.add(this.Pause_Resume, this);
        this.createBounders();
        this.createPlayer();
        this.createTextsBoard();
    },
    update: function () {
        if (status == 'gameOver' && keyboard.enter.isDown) this.restart();
        else if (status != 'running') {
            return;
        }
        else if (status == 'running' && !PressPause) {
            this.physics.arcade.collide(player, platforms, this.effect);
            this.physics.arcade.collide(player, [leftWall, rightWall]);
            this.physics.arcade.collide(player, [leftWall2, rightWall2]);
            checkTouchCeiling(player);
            this.checkGameOver();
            this.updatePlayer();
            this.updatePlatforms();
            this.updateTextsBoard();
            this.createPlatforms();
            text1.visible = true;
            text2.visible = true;
            resume_text.visible = false;
        }
        else if (status == 'running' && PressPause) {
            this.physics.arcade.collide(player, platforms, this.effect);
            this.physics.arcade.collide(player, [leftWall, rightWall]);
            this.physics.arcade.collide(player, [leftWall2, rightWall2]);
            text1.visible = false;
            text2.visible = false;
            resume_text.visible = true;
            lastTime = game.time.now;
            this.updatePlayer();
        }
    },
    createBounders: function () {
        leftWall = game.add.sprite(0, 0, 'wall');
        game.physics.arcade.enable(leftWall);
        leftWall.body.immovable = true;

        rightWall = game.add.sprite(583, 0, 'wall');
        game.physics.arcade.enable(rightWall);
        rightWall.body.immovable = true;

        leftWall2 = game.add.sprite(0, 400, 'wall');
        game.physics.arcade.enable(leftWall2);
        leftWall2.body.immovable = true;

        rightWall2 = game.add.sprite(583, 400, 'wall');
        game.physics.arcade.enable(rightWall2);
        rightWall2.body.immovable = true;

        ceiling = game.add.image(0, 0, 'ceiling');
        ceiling2 = game.add.image(400, 0, 'ceiling');
    },


    createPlatforms: function () {
        if (game.time.now > lastTime + 600) {
            lastTime = game.time.now;
            this.createOnePlatform();
            distance += 1;
        }
    },

    createOnePlatform: function () {

        var platform;
        var x = Math.random() * (600 - 96 - 40) + 20;
        var y = 700;
        var rand = Math.random() * 100;
        if (distance < 1) {
            platform = game.add.sprite(game.width / 2 - 100, game.height / 2, 'normal');
        }
        else if (rand < 20 && distance >= 1) {
            platform = game.add.sprite(x, y, 'normal');
        } else if (rand < 40 && distance >= 1) {
            platform = game.add.sprite(x, y, 'nails');
            game.physics.arcade.enable(platform);
            platform.body.setSize(96, 15, 0, 15);
        } else if (rand < 50 && distance >= 1) {
            platform = game.add.sprite(x, y, 'conveyorLeft');
            platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
            platform.play('scroll');
        } else if (rand < 60 && distance >= 1) {
            platform = game.add.sprite(x, y, 'conveyorRight');
            platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
            platform.play('scroll');
        } else if (rand < 80 && distance >= 1) {
            platform = game.add.sprite(x, y, 'trampoline');
            platform.animations.add('jump', [4, 5, 4, 3, 2, 1, 0, 1, 2, 3], 120);
            platform.frame = 3;
        } else {
            platform = game.add.sprite(x, y, 'fake');
            platform.animations.add('turn', [0, 1, 2, 3, 4, 5, 0], 14);
        }

        game.physics.arcade.enable(platform);
        platform.body.immovable = true;
        platforms.push(platform);

        platform.body.checkCollision.down = false;
        platform.body.checkCollision.left = false;
        platform.body.checkCollision.right = false;
    },

    createPlayer: function () {
        player = game.add.sprite(200, 50, 'player');
        player.direction = 10;
        game.physics.arcade.enable(player);
        player.body.gravity.y = 500;
        player.animations.add('left', [0, 1, 2, 3], 8);
        player.animations.add('right', [9, 10, 11, 12], 8);
        player.animations.add('flyleft', [18, 19, 20, 21], 12);
        player.animations.add('flyright', [27, 28, 29, 30], 12);
        player.animations.add('fly', [36, 37, 38, 39], 12);
        player.life = 1000;
        player.unbeatableTime = 0;
        player.touchOn = undefined;
    },

    createTextsBoard: function () {
        var style = { fill: '#ff0000', fontSize: '25px' };
        var style_1 = { fill: '#ffff37', fontSize: '30px' };
        var style_2 = { fill: '#bebebe', fontSize: '30px' };
        var style_3 = { fill: '#842b00', fontSize: '30px' };
        var style_leaderboard = { fill: '#ff0000', fontSize: '55px' };
        text1 = game.add.text(50, 10, '', style);
        text2 = game.add.text(450, 10, '', style);
        text3 = game.add.text(180, 300, 'Enter to Restart', style);
        resume_text = game.add.text(game.width / 2 - 100, game.height / 2, 'Press Space to Resume', style);
        resume_text.visible = false;
        text3.visible = false;
    },

    updatePlayer: function () {
        if (PressPause) {
            player.body.velocity.y = 0;
            player.body.velocity.x = 0;
            player.body.gravity.y = 0;
        }
        else {
            player.body.gravity.y = 500;
            if (keyboard.left.isDown || keyboard.a.isDown) {
                player.body.velocity.x = -250;
            } else if (keyboard.right.isDown || keyboard.d.isDown) {
                player.body.velocity.x = 250;
            } else {
                player.body.velocity.x = 0;
            }
            this.setPlayerAnimate(player);
        }
    },

    setPlayerAnimate: function (player) {
        var x = player.body.velocity.x;
        var y = player.body.velocity.y;
        if (x < 0 && y > 0) {
            player.animations.play('flyleft');
        }
        if (x > 0 && y > 0) {
            player.animations.play('flyright');
        }
        if (x < 0 && y == 0) {
            if (runningmusic.isPlaying) {
                runningmusic.resume();
            } else {
                runningmusic.play();
            }
            player.animations.play('left');
        }
        if (x > 0 && y == 0) {
            if (runningmusic.isPlaying) {
                runningmusic.resume();
            } else {
                runningmusic.play();
            }
            player.animations.play('right');
        }
        if (x == 0 && y != 0) {
            player.animations.play('fly');
        }
        if (x == 0 && y == 0) {
            if (runningmusic.isPlaying) {
                runningmusic.stop();
            }
            player.frame = 8;
        }
    },

    updatePlatforms: function () {
        for (var i = 0; i < platforms.length; i++) {
            var platform = platforms[i];
            platform.body.position.y -= 2;
            if (platform.body.position.y <= -20) {
                platform.destroy();
                platforms.splice(i, 1);
            }
        }
    },

    updateTextsBoard: function () {
        text1.setText('Life: ' + player.life);
        text2.setText('Score: ' + distance);
    },

    effect: function (player, platform) {
        if (platform.key == 'conveyorRight') {
            conveyorRightEffect(player, platform);
        }
        if (platform.key == 'conveyorLeft') {
            conveyorLeftEffect(player, platform);
        }
        if (platform.key == 'trampoline') {
            trampolineEffect(player, platform);
        }
        if (platform.key == 'nails') {
            nailsEffect(player, platform);
        }
        if (platform.key == 'normal') {
            basicEffect(player, platform);
        }
        if (platform.key == 'fake') {
            fakeEffect(player, platform);
        }
    },

    checkGameOver: function () {
        if (player.life <= 0 || player.body.y > 800) {
            this.gameOver();
        }
    },

    gameOver: function () {
        writeData();
        text3.visible = true;
        platforms.forEach(function (s) { s.destroy() });
        platforms = [];
        game.sound.stopAll();
        gameovermusic.play();
        status = 'gameOver';
    },

    restart: function () {
        text3.visible = false;
        distance = 0;
        this.createPlayer();
        status = 'running';
    },
    Pause_Resume: function () {
        if (PressPause) {
            PressPause = false;
        }
        else {
            runningmusic.pause();
            PressPause = true;
        }
    },
    back_to_menu: function () {
        platforms.forEach(function (s) { s.destroy() });
        platforms = [];
        distance = 0;
        game.sound.stopAll();
        status = 'running';
        game.state.start('menu');
    }
}
var Play2_State = {
    preload: function () {
        game.load.crossOrigin = 'anonymous';
        game.load.spritesheet('player', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/player.png', 32, 32);
        game.load.image('wall', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/wall.png');
        game.load.image('ceiling', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/ceiling.png');
        game.load.image('normal', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/normal.png');
        game.load.image('nails', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/nails.png');
        game.load.spritesheet('conveyorRight', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/conveyor_right.png', 96, 16);
        game.load.spritesheet('conveyorLeft', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/conveyor_left.png', 96, 16);
        game.load.spritesheet('trampoline', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/trampoline.png', 96, 22);
        game.load.spritesheet('fake', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/fake.png', 96, 36);
        game.load.audio('menu_music', 'assets/y2mate.com - motivational_background_music_J0BPoofmPkw.mp3');
        game.load.audio('gameover_music', 'assets/falling2a.mp3');
        game.load.audio('jump_music', 'assets/jump.mp3');
        game.load.audio('running_music', 'assets/running_in_a_hall.mp3');
        game.load.audio('fake_music', 'assets/putting_a_large_bag2.mp3');
        game.load.audio('spring_music', 'assets/question.mp3');
        game.load.audio('scream_music','assets/scream.mp3');
    },

    create: function () {
        screammusic = game.add.audio('scream_music');
        gameovermusic = game.add.audio('gameover_music');
        jumpmusic = game.add.audio('jump_music');
        runningmusic = game.add.audio('running_music');
        fakemusic = game.add.audio('fake_music');
        springmusic = game.add.audio('spring_music');
        
        keyboard = game.input.keyboard.addKeys({
            'enter': Phaser.Keyboard.ENTER,
            'up': Phaser.Keyboard.UP,
            'down': Phaser.Keyboard.DOWN,
            'left': Phaser.Keyboard.LEFT,
            'right': Phaser.Keyboard.RIGHT,
            'w': Phaser.Keyboard.W,
            'a': Phaser.Keyboard.A,
            's': Phaser.Keyboard.S,
            'd': Phaser.Keyboard.D,
            'P': Phaser.Keyboard.P,
            'R': Phaser.Keyboard.R,
            'Space': Phaser.Keyboard.SPACEBAR,
            'Esc': Phaser.Keyboard.ESC
        });
        game.input.keyboard.addKey(Phaser.Keyboard.ESC).onDown.add(this.back_to_menu, this);
        game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR).onDown.add(this.Pause_Resume, this);
        this.createBounders();
        this.createPlayer();
        this.createTextsBoard();
    },
    update: function () {
        if (status == 'gameOver' && keyboard.enter.isDown) this.restart();
        else if (status != 'running') {
            return;
        }
        else if (status == 'running' && !PressPause) {
            this.physics.arcade.collide(player, player2);
            this.physics.arcade.collide(player, platforms, this.effect);
            this.physics.arcade.collide(player, [leftWall, rightWall]);
            this.physics.arcade.collide(player, [leftWall2, rightWall2]);
            checkTouchCeiling(player);
            //player2
            this.physics.arcade.collide(player2, platforms, this.effect);
            this.physics.arcade.collide(player2, [leftWall, rightWall]);
            this.physics.arcade.collide(player2, [leftWall2, rightWall2]);
            checkTouchCeiling(player2);

            this.checkGameOver();
            this.updatePlayer();
            this.updatePlatforms();
            this.updateTextsBoard();
            this.createPlatforms();
            resume_text.visible = false;
        }
        else if (status == 'running' && PressPause) {
            this.physics.arcade.collide(player, player2);
            this.physics.arcade.collide(player, platforms, this.effect);
            this.physics.arcade.collide(player, [leftWall, rightWall]);
            this.physics.arcade.collide(player, [leftWall2, rightWall2]);
            //player2
            this.physics.arcade.collide(player2, platforms, this.effect2);
            this.physics.arcade.collide(player2, [leftWall, rightWall]);
            this.physics.arcade.collide(player2, [leftWall2, rightWall2]);
            resume_text.visible = true;
            lastTime = game.time.now;
            this.updatePlayer();
        }
    },
    createBounders: function () {
        leftWall = game.add.sprite(0, 0, 'wall');
        game.physics.arcade.enable(leftWall);
        leftWall.body.immovable = true;

        rightWall = game.add.sprite(583, 0, 'wall');
        game.physics.arcade.enable(rightWall);
        rightWall.body.immovable = true;

        leftWall2 = game.add.sprite(0, 400, 'wall');
        game.physics.arcade.enable(leftWall2);
        leftWall2.body.immovable = true;

        rightWall2 = game.add.sprite(583, 400, 'wall');
        game.physics.arcade.enable(rightWall2);
        rightWall2.body.immovable = true;

        ceiling = game.add.image(0, 0, 'ceiling');
        ceiling2 = game.add.image(400, 0, 'ceiling');
    },


    createPlatforms: function () {
        if (game.time.now > lastTime + 600) {
            lastTime = game.time.now;
            this.createOnePlatform();
            distance += 1;
            distance2 += 1;
        }
    },

    createOnePlatform: function () {

        var platform;
        var x = Math.random() * (600 - 96 - 40) + 20;
        var y = 700;
        var rand = Math.random() * 100;
        if (distance < 1) {
            platform = game.add.sprite(game.width / 2 - 100, game.height / 2, 'normal');
        }
        else if (rand < 20 && distance >= 1) {
            platform = game.add.sprite(x, y, 'normal');
        } else if (rand < 40 && distance >= 1) {
            platform = game.add.sprite(x, y, 'nails');
            game.physics.arcade.enable(platform);
            platform.body.setSize(96, 15, 0, 15);
        } else if (rand < 50 && distance >= 1) {
            platform = game.add.sprite(x, y, 'conveyorLeft');
            platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
            platform.play('scroll');
        } else if (rand < 60 && distance >= 1) {
            platform = game.add.sprite(x, y, 'conveyorRight');
            platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
            platform.play('scroll');
        } else if (rand < 80 && distance >= 1) {
            platform = game.add.sprite(x, y, 'trampoline');
            platform.animations.add('jump', [4, 5, 4, 3, 2, 1, 0, 1, 2, 3], 120);
            platform.frame = 3;
        } else {
            platform = game.add.sprite(x, y, 'fake');
            platform.animations.add('turn', [0, 1, 2, 3, 4, 5, 0], 14);
        }

        game.physics.arcade.enable(platform);
        platform.body.immovable = true;
        platforms.push(platform);

        platform.body.checkCollision.down = false;
        platform.body.checkCollision.left = false;
        platform.body.checkCollision.right = false;
    },

    createPlayer: function () {
        player = game.add.sprite(300, 50, 'player');
        player.direction = 10;
        game.physics.arcade.enable(player);
        player.body.gravity.y = 500;
        player.animations.add('left', [0, 1, 2, 3], 8);
        player.animations.add('right', [9, 10, 11, 12], 8);
        player.animations.add('flyleft', [18, 19, 20, 21], 12);
        player.animations.add('flyright', [27, 28, 29, 30], 12);
        player.animations.add('fly', [36, 37, 38, 39], 12);
        player.life = 1000;
        player.unbeatableTime = 0;
        player.touchOn = undefined;
        //player2
        player2 = game.add.sprite(200, 50, 'player2');
        player2.direction = 10;
        game.physics.arcade.enable(player2);
        player2.body.gravity.y = 500;
        player2.animations.add('left', [0, 1, 2, 3], 8);
        player2.animations.add('right', [9, 10, 11, 12], 8);
        player2.animations.add('flyleft', [18, 19, 20, 21], 12);
        player2.animations.add('flyright', [27, 28, 29, 30], 12);
        player2.animations.add('fly', [36, 37, 38, 39], 12);
        player2.life = 1000;
        player2.unbeatableTime = 0;
        player2.touchOn = undefined;
    },

    createTextsBoard: function () {
        var style = { fill: '#ff0000', fontSize: '25px' };
        var winner_style = { fill: '#ffffff', fontSize: '40px' };
        var playername_style = { fill: '#ffffff', fontSize: '20px' };
        text3 = game.add.text(180, 300, 'Enter to Restart', style);
        winner_text = game.add.text(180, 380, '', winner_style);
        // note_player1 = game.add.text(player.body.x + 5, player.body.y - 10, '1', playername_style);
        // note_player2 = game.add.text(player2.body.x + 5, player2.body.y - 10, '2', playername_style);
        resume_text = game.add.text(game.width / 2 - 100, game.height / 2, 'Press Space to Resume', style);
        resume_text.visible = false;
        text3.visible = false;
        winner_text.visible = false;
    },

    updateTextsBoard: function () {
        var playername_style = { fill: '#ffffff', fontSize: '20px' };
        // note_player1.setText('');
        // note_player2.setText('');
        // note_player1 = game.add.text(player.body.x + 5, player.body.y - 10, '1', playername_style);
        // note_player2 = game.add.text(player2.body.x + 5, player2.body.y - 10, '2', playername_style);
    },

    updatePlayer: function () {
        if (PressPause) {
            player.body.velocity.y = 0;
            player.body.velocity.x = 0;
            player.body.gravity.y = 0;
            //player2
            player2.body.velocity.y = 0;
            player2.body.velocity.x = 0;
            player2.body.gravity.y = 0;
        }
        else {
            player.body.gravity.y = 500;
            if (keyboard.left.isDown) {
                player.body.velocity.x = -250;
            } else if (keyboard.right.isDown) {
                player.body.velocity.x = 250;
            } else {
                player.body.velocity.x = 0;
            }
            this.setPlayerAnimate(player);
            //player2
            player2.body.gravity.y = 500;
            if (keyboard.a.isDown) {
                player2.body.velocity.x = -250;
            } else if (keyboard.d.isDown) {
                player2.body.velocity.x = 250;
            } else {
                player2.body.velocity.x = 0;
            }
            this.setPlayerAnimate(player2);
        }
    },

    setPlayerAnimate: function (player) {
        var x = player.body.velocity.x;
        var y = player.body.velocity.y;
        var x1 = player2.body.velocity.x;
        var y1 = player2.body.velocity.y;
        if (x < 0 && y > 0) {
            player.animations.play('flyleft');
        }
        if (x > 0 && y > 0) {
            player.animations.play('flyright');
        }
        if (x < 0 && y == 0) {
            player.animations.play('left');
        }
        if (x > 0 && y == 0) {
            player.animations.play('right');
        }
        if (x == 0 && y != 0) {
            player.animations.play('fly');
        }
        if (x == 0 && y == 0) {
            player.frame = 8;
        }
        //player2
        if (x1 < 0 && y1 > 0) {
            player2.animations.play('flyleft');
        }
        if (x1 > 0 && y1 > 0) {
            player2.animations.play('flyright');
        }
        if (x1 < 0 && y1 == 0) {
            player2.animations.play('left');
        }
        if (x1 > 0 && y1 == 0) {
            player2.animations.play('right');
        }
        if (x1 == 0 && y1 != 0) {
            player2.animations.play('fly');
        }
        if (x1 == 0 && y1 == 0) {
            player2.frame = 8;
        }
    },

    updatePlatforms: function () {
        for (var i = 0; i < platforms.length; i++) {
            var platform = platforms[i];
            platform.body.position.y -= 2;
            if (platform.body.position.y <= -20) {
                platform.destroy();
                platforms.splice(i, 1);
            }
        }
    },

    effect: function (player, platform) {
        if (platform.key == 'conveyorRight') {
            conveyorRightEffect(player, platform);
        }
        if (platform.key == 'conveyorLeft') {
            conveyorLeftEffect(player, platform);
        }
        if (platform.key == 'trampoline') {
            trampolineEffect(player, platform);
        }
        if (platform.key == 'nails') {
            nailsEffect(player, platform);
        }
        if (platform.key == 'normal') {
            basicEffect(player, platform);
        }
        if (platform.key == 'fake') {
            fakeEffect(player, platform);
        }
    },
    effect2: function (player2, platform) {
        if (platform.key == 'conveyorRight') {
            conveyorRightEffect(player2, platform);
        }
        if (platform.key == 'conveyorLeft') {
            conveyorLeftEffect(player2, platform);
        }
        if (platform.key == 'trampoline') {
            trampolineEffect(player2, platform);
        }
        if (platform.key == 'nails') {
            nailsEffect(player2, platform);
        }
        if (platform.key == 'normal') {
            basicEffect(player2, platform);
        }
        if (platform.key == 'fake') {
            fakeEffect(player2, platform);
        }
    },

    checkGameOver: function () {
        if (player.body.y > 800 || player2.body.y > 800) {
            if (player.body.y > 800) {
                winner_text.setText('Player2 is Winner!');
                winner_text.visible = true;
                // note_player1.visible = false;
                // note_player2.visible = false;
            } else if (player2.body.y > 800) {
                winner_text.setText('Player1 is Winner!');
                winner_text.visible = true;
                // note_player1.visible = false;
                // note_player2.visible = false;
            }
            this.gameOver();
        }
    },

    gameOver: function () {
        text3.visible = true;
        platforms.forEach(function (s) { s.destroy() });
        platforms = [];
        game.sound.stopAll();
        gameovermusic.play();
        status = 'gameOver';
    },

    restart: function () {
        text3.visible = false;
        winner_text.visible = false;
        // note_player1.visible = true;
        // note_player2.visible = true;
        distance = 0;
        this.createPlayer();
        status = 'running';
    },
    Pause_Resume: function () {
        if (PressPause) {
            PressPause = false;
        }
        else {
            runningmusic.pause();
            PressPause = true;
        }
    },
    back_to_menu: function () {
        platforms.forEach(function (s) { s.destroy() });
        platforms = [];
        distance = 0;
        game.sound.stopAll();
        status = 'running';
        game.state.start('menu');
    }
}
var LeaderBoard_State = {
    preload: function () {
        game.load.crossOrigin = 'anonymous';
        game.load.spritesheet('player', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/player.png', 32, 32);
        game.load.image('wall', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/wall.png');
        game.load.image('ceiling', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/ceiling.png');
        game.load.image('normal', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/normal.png');
        game.load.image('nails', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/nails.png');
        game.load.spritesheet('conveyorRight', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/conveyor_right.png', 96, 16);
        game.load.spritesheet('conveyorLeft', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/conveyor_left.png', 96, 16);
        game.load.spritesheet('trampoline', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/trampoline.png', 96, 22);
        game.load.spritesheet('fake', 'https://wacamoto.github.io/NS-Shaft-Tutorial/assets/fake.png', 96, 36);
        game.load.audio('menu_music', 'assets/y2mate.com - motivational_background_music_J0BPoofmPkw.mp3');
        game.load.audio('gameover_music', 'assets/falling2a.mp3');
        game.load.audio('jump_music', 'assets/jump04.mp3');
        game.load.audio('running_music', 'assets/running_in_a_hall.mp3');
        game.load.audio('fake_music', 'assets/putting_a_large_bag2.mp3');
        game.load.audio('spring_music', 'assets/question.mp3');
    },

    create: function () {
        leaderboardmusic.play();
        cover = game.add.image(0, 0, 'powerstation');
        cover.scale.setTo(0.8, 0.8);
        gameovermusic = game.add.audio('gameover_music');
        jumpmusic = game.add.audio('jump_music');
        runningmusic = game.add.audio('running_music');
        fakemusic = game.add.audio('fake_music');
        springmusic = game.add.audio('spring_music');
        keyboard = game.input.keyboard.addKeys({
            'enter': Phaser.Keyboard.ENTER,
            'up': Phaser.Keyboard.UP,
            'down': Phaser.Keyboard.DOWN,
            'left': Phaser.Keyboard.LEFT,
            'right': Phaser.Keyboard.RIGHT,
            'w': Phaser.Keyboard.W,
            'a': Phaser.Keyboard.A,
            's': Phaser.Keyboard.S,
            'd': Phaser.Keyboard.D,
            'P': Phaser.Keyboard.P,
            'R': Phaser.Keyboard.R,
            'Space': Phaser.Keyboard.SPACEBAR,
            'Esc': Phaser.Keyboard.ESC
        });
        game.input.keyboard.addKey(Phaser.Keyboard.ESC).onDown.add(this.back_to_menu, this);
        this.createBounders();
        this.createTextsBoard();
        leaderboard_firstplace.setText('1. ' + firstplace_name + ' : ' + firstplace_record);
        leaderboard_secondplace.setText('2. ' + secondplace_name + ' : ' + secondplace_record);
        leaderboard_thirdplace.setText('3. ' + thirdplace_name + ' : ' + thirdplace_record);
        leaderboard_firstplace_time.setText('Time : ' + firstplace_time);
        leaderboard_secondplace_time.setText('Time : ' + secondplace_time);
        leaderboard_thirdplace_time.setText('Time : ' + thirdplace_time);
        leaderboard_text.setText('LEADERBOARD');
        leaderboard_firstplace.visible = true;
        leaderboard_secondplace.visible = true;
        leaderboard_thirdplace.visible = true;
        leaderboard_firstplace_time.visible = true;
        leaderboard_secondplace_time.visible = true;
        leaderboard_thirdplace_time.visible = true;
        leaderboard_text.visible = true;
    },
    createBounders: function () {
        leftWall = game.add.sprite(0, 0, 'wall');
        game.physics.arcade.enable(leftWall);
        leftWall.body.immovable = true;

        rightWall = game.add.sprite(583, 0, 'wall');
        game.physics.arcade.enable(rightWall);
        rightWall.body.immovable = true;

        leftWall2 = game.add.sprite(0, 400, 'wall');
        game.physics.arcade.enable(leftWall2);
        leftWall2.body.immovable = true;

        rightWall2 = game.add.sprite(583, 400, 'wall');
        game.physics.arcade.enable(rightWall2);
        rightWall2.body.immovable = true;
    },

    createTextsBoard: function () {
        var style_1 = { fill: '#ffff37', fontSize: '30px' };
        var style_2 = { fill: '#bebebe', fontSize: '30px' };
        var style_3 = { fill: '#842b00', fontSize: '30px' };
        var time_style = { fill: '#ffffff', fontSize: '12px' };
        var style_leaderboard = { fill: '#ff0000', fontSize: '55px' };
        leaderboard_firstplace = game.add.text(50, game.height / 2 + 140, '', style_1);
        leaderboard_secondplace = game.add.text(50, game.height / 2 + 210, '', style_2);
        leaderboard_thirdplace = game.add.text(50, game.height / 2 + 280, '', style_3);
        leaderboard_firstplace_time = game.add.text(game.width - 200, game.height / 2 + 170, '', time_style);
        leaderboard_secondplace_time = game.add.text(game.width - 200, game.height / 2 + 240, '', time_style);
        leaderboard_thirdplace_time = game.add.text(game.width - 200, game.height / 2 + 310, '', time_style);
        leaderboard_text = game.add.text(50, game.height / 2 + 70, '', style_leaderboard);
        leaderboard_firstplace.visible = false;
        leaderboard_secondplace.visible = false;
        leaderboard_thirdplace.visible = false;
        leaderboard_firstplace_time.visible = false;
        leaderboard_secondplace_time.visible = false;
        leaderboard_thirdplace_time.visible = false;
        leaderboard_text.visible = false;
    },
    back_to_menu: function () {
        leaderboard_firstplace.visible = false;
        leaderboard_secondplace.visible = false;
        leaderboard_thirdplace.visible = false;
        leaderboard_firstplace_time.visible = false;
        leaderboard_secondplace_time.visible = false;
        leaderboard_thirdplace_time.visible = false;
        leaderboard_text.visible = false;
        platforms.forEach(function (s) { s.destroy() });
        platforms = [];
        distance = 0;
        game.sound.stopAll();
        status = 'running';
        game.state.start('menu');
    }
}
function conveyorRightEffect(player, platform) {
    player.body.x += 2;
}

function conveyorLeftEffect(player, platform) {
    player.body.x -= 2;
}

function trampolineEffect(player, platform) {
    jumpmusic.play();
    platform.animations.play('jump');
    player.body.velocity.y = -350;
}

function nailsEffect(player, platform) {
    if (player.touchOn !== platform) {
        screammusic.play();
        player.life -= 3;
        player.touchOn = platform;
        game.camera.flash(0xff0000, 100);
    }
}

function basicEffect(player, platform) {
    if (player.touchOn !== platform) {
        if (player.life < 10) {
            player.life += 1;
        }
        player.touchOn = platform;
    }
}

function fakeEffect(player, platform) {
    if (player.touchOn !== platform) {
        platform.animations.play('turn');
        setTimeout(function () {
            platform.body.checkCollision.up = false;
        }, 100);
        player.touchOn = platform;
    }
}

function checkTouchCeiling(player) {
    if (player.body.y < 0) {
        if (player.body.velocity.y < 0) {
            player.body.velocity.y = 0;
        }
        if (game.time.now > player.unbeatableTime) {
            player.life -= 3;
            game.camera.flash(0xff0000, 100);
            player.unbeatableTime = game.time.now + 2000;
        }
    }
}
function writeData() {
    if (number_of_users < 3 || distance > thirdplace_record) {
        var Renew = new Date();
        if (distance > firstplace_record) {
            alert('You are in first place now.');
        } else if (distance > secondplace_record && distance < firstplace_record) {
            alert('You are in second place now.')
        } else if (distance > thirdplace_record && distance < secondplace_record) {
            alert('You are in third place now.');
        }
        var user_name = (firebase.auth().currentUser.displayName != null) ? firebase.auth().currentUser.displayName : prompt("Please Enter Your Name: ", "");
        var newpostref = firebase.database().ref('Record').push();
        newpostref.set({
            email: user_name,
            data: distance,
            Year: Renew.getFullYear(),
            Month: Renew.getMonth() + 1,
            Day: Renew.getDate(),
            Hours: Renew.getHours(),
            Minutes: Renew.getMinutes(),
            Seconds: Renew.getSeconds()
        });
    }
}
function getranking() {
    var postsRef = firebase.database().ref('Record');
    var total_post = [];
    var name_post = [];
    var time_post = [];
    var first_count = 0;
    var second_count = 0;
    postsRef.once('value')
        .then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                var childData = childSnapshot.val();
                var name = childSnapshot.val().email;
                var bestrecord = childSnapshot.val().data;
                var year = childSnapshot.val().Year;
                var month = childSnapshot.val().Month;
                var day = childSnapshot.val().Day;
                var hours = childSnapshot.val().Hours;
                var minutes = childSnapshot.val().Minutes;
                var seconds = childSnapshot.val().Seconds;
                if (hours < 10)
                    hours = '0' + hours;
                if (minutes < 10)
                    minutes = '0' + minutes;
                if (seconds < 10)
                    seconds = '0' + seconds;
                total_post[total_post.length] = bestrecord;
                name_post[name_post.length] = name;
                time_post[time_post.length] = year + " / " + month + " / " + day + " " + hours + " : " + minutes + " : " + seconds;
                first_count += 1;
            });

            //add listener
            postsRef.on('child_added', function (data) {
                second_count += 1;
                if (second_count > first_count) {
                    var childData = data.val();
                    var name = data.val().email;
                    var bestrecord = data.val().data;
                    var year = data.val().Year;
                    var month = data.val().Month;
                    var day = data.val().Day;
                    var hours = data.val().Hours;
                    var minutes = data.val().Minutes;
                    var seconds = data.val().Seconds;
                    if (hours < 10)
                        hours = '0' + hours;
                    if (minutes < 10)
                        minutes = '0' + minutes;
                    if (seconds < 10)
                        seconds = '0' + seconds;
                    total_post[total_post.length] = bestrecord;
                    name_post[name_post.length] = name;
                    time_post[time_post.length] = year + " / " + month + " / " + day + " " + hours + " : " + minutes + " : " + seconds;
                }
            });
            for (var i = total_post.length - 1; i > 0; i--) {
                for (var j = 0; j < i; j++) {
                    if (total_post[j] < total_post[j + 1]) {
                        var tmp, tmp_name, tmp_time;

                        tmp = total_post[j];
                        total_post[j] = total_post[j + 1];
                        total_post[j + 1] = tmp;

                        tmp_name = name_post[j];
                        name_post[j] = name_post[j + 1];
                        name_post[j + 1] = tmp_name;

                        tmp_time = time_post[j];
                        time_post[j] = time_post[j + 1];
                        time_post[j + 1] = tmp_time;
                    }
                }
            }
            firstplace_name = name_post[0];
            firstplace_record = total_post[0];
            firstplace_time = time_post[0];
            secondplace_name = name_post[1];
            secondplace_record = total_post[1];
            secondplace_time = time_post[1];
            thirdplace_name = name_post[2];
            thirdplace_record = total_post[2];
            thirdplace_time = time_post[2];
            number_of_users = total_post.length;
            console.log('lenght: ' + total_post.length);
            console.log('1.' + firstplace_name + ':' + firstplace_record + '->' + firstplace_time);
            console.log('2.' + secondplace_name + ':' + secondplace_record + '->' + secondplace_time);
            console.log('3.' + thirdplace_name + ':' + thirdplace_record + '->' + thirdplace_time);
        })
        .catch(e => console.log(e.message));
}
game.state.add('menu', Menu_State);
game.state.add('play', Play_State);
game.state.add('play2', Play2_State);
game.state.add('leaderboard', LeaderBoard_State);
game.state.start('menu');