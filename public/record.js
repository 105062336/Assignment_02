var firstplace_name = "null";
var secondplace_name = "null" ;
var thirdplace_name = "null" ;
var firstplace_record = 0;
var secondplace_record = 0;
var thirdplace_record = 0;
$(function () {
    var Today = new Date();
    var user = firebase.auth().currentUser;
    function writeData() {
        if (post_txt.value != "") {
            var Renew = new Date();
            var newpostref = firebase.database().ref('Record').push();
            newpostref.set({
                email: firebase.auth().currentUser.displayName,
                data: distance,
                Year: Renew.getFullYear(),
                Month: Renew.getMonth() + 1,
                Day: Renew.getDate(),
                Hours: Renew.getHours(),
                Minutes: Renew.getMinutes(),
                Seconds: Renew.getSeconds()
            });
        }
    }
    var postsRef = firebase.database().ref('Record');
    var total_post = [];
    var name_post = [];
    var first_count = 0;
    var second_count = 0;
    postsRef.once('value')
        .then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                var childData = childSnapshot.val();
                var name = childSnapshot.val().email;
                var bestrecord = childSnapshot.val().data;
                var year = childSnapshot.val().Year;
                var month = childSnapshot.val().Month;
                var day = childSnapshot.val().Day;
                var hours = childSnapshot.val().Hours;
                var minutes = childSnapshot.val().Minutes;
                var seconds = childSnapshot.val().Seconds;
                if (hours < 10)
                    hours = '0' + hours;
                if (minutes < 10)
                    minutes = '0' + minutes;
                if (seconds < 10)
                    seconds = '0' + seconds;
                total_post[total_post.length] = bestrecord;
                name_post[total_post.length] = name;
                first_count += 1;
            });

            //add listener
            postsRef.on('child_added', function (data) {
                second_count += 1;
                if (second_count > first_count) {
                    var childData = data.val();
                    var name = data.val().email;
                    var bestrecord = data.val().data;
                    var year = data.val().Year;
                    var month = data.val().Month;
                    var day = data.val().Day;
                    var hours = data.val().Hours;
                    var minutes = data.val().Minutes;
                    var seconds = data.val().Seconds;
                    if (hours < 10)
                        hours = '0' + hours;
                    if (minutes < 10)
                        minutes = '0' + minutes;
                    if (seconds < 10)
                        seconds = '0' + seconds;
                    total_post[total_post.length] = bestrecord;
                    name_post[total_post.length] = name;
                }
            });
            for(var i = total_post.length;i>0;i--){
                for(var j = 0 ; j<i ;j++){
                    if(total_post[j]<total_post[j+1]){
                        var tmp,tmp_name;
                        tmp = total_post[j];
                        total_post[j] = total_post[j+1];
                        total_post[j+1] = tmp;
                        tmp_name = name_post[j];
                        name_post[j] = name_post[j+1];
                        name_post[j+1] = tmp_name;
                    }
                }
            }
        })
        .catch(e => console.log(e.message));
});
